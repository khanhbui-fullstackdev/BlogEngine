namespace BlogEngine.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_ReplyComment_AddProperty_Name_UserName_Email : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ReplyComments", "Name", c => c.String(maxLength: 255));
            AddColumn("dbo.ReplyComments", "UserName", c => c.String(maxLength: 300, unicode: false));
            AddColumn("dbo.ReplyComments", "Email", c => c.String(maxLength: 50, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ReplyComments", "Email");
            DropColumn("dbo.ReplyComments", "UserName");
            DropColumn("dbo.ReplyComments", "Name");
        }
    }
}
