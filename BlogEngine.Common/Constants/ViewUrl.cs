﻿namespace BlogEngine.Common.Constants
{
    public static class ViewUrl
    {
        public const string _CommentArea = "~/Views/Post/CommentPanel/_CommentArea.cshtml";
        public const string _CommentText = "~/Views/Post/CommentPanel/_CommentText.cshtml";

        public const string _ReplyCommentArea = "~/Views/Comment/ReplyCommentPanel/_ReplyCommentArea.cshtml";
        public const string _ReplyCommentText = "~/Views/Comment/ReplyCommentPanel/_ReplyCommentText.cshtml";
    }
}
