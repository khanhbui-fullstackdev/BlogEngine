namespace BlogEngine.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Comment_UpdateProperties_Name_UserName_Content_Email : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Comments", "Name", c => c.String(maxLength: 255));
            AlterColumn("dbo.Comments", "UserName", c => c.String(maxLength: 300, unicode: false));
            AlterColumn("dbo.Comments", "Content", c => c.String());
            AlterColumn("dbo.Comments", "Email", c => c.String(maxLength: 50, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Comments", "Email", c => c.String(nullable: false, maxLength: 50, unicode: false));
            AlterColumn("dbo.Comments", "Content", c => c.String(nullable: false, maxLength: 4000));
            AlterColumn("dbo.Comments", "UserName", c => c.String(nullable: false, maxLength: 300, unicode: false));
            AlterColumn("dbo.Comments", "Name", c => c.String(nullable: false, maxLength: 255));
        }
    }
}
