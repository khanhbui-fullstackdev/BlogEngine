﻿using BlogEngine.Common.Constants;
using BlogEngine.Model.Abstracts;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogEngine.Model.Models
{
    [Table("Comments")]
    public class Comment : Audit
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { set; get; }

        [StringLength(255), Column(TypeName = "nvarchar")]
        public string Name { get; set; }

        [StringLength(300), Column(TypeName = "varchar")]
        public string UserName { get; set; }

        public string Content { get; set; }

        [EmailAddress(ErrorMessage = ErrorMessage.InvalidEmail), StringLength(50), Column(TypeName = "varchar")]
        public string Email { get; set; }
       
        public int PostID { get; set; }

        [ForeignKey("PostID")]
        public virtual Post Post { get; set; }

        public int CountReplyComments { get; set; } = 0;

        public int CountLikes { get; set; } = 0;
        
        public virtual IEnumerable<ReplyComment> ReplyComments { get; set; }
    }
}
