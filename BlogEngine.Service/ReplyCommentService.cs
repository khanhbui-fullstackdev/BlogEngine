﻿using BlogEngine.Data.Infrastrutures;
using BlogEngine.Data.Repositories.IRepositories;
using BlogEngine.Model.Models;
using BlogEngine.Service.IServices;
using System;
using System.Collections.Generic;

namespace BlogEngine.Service
{
    public class ReplyCommentService : IReplyCommentService
    {
        private IReplyCommentRepository _replyCommentRepository;
        private IUnitOfWork _unitOfWork;

        public ReplyCommentService(IReplyCommentRepository replyCommentRepository, IUnitOfWork unitOfWork)
        {
            this._replyCommentRepository = replyCommentRepository;
            this._unitOfWork = unitOfWork;
        }

        public ReplyComment AddNewComment(ReplyComment replyComment)
        {
            return _replyCommentRepository.Add(replyComment);
        }

        public IEnumerable<ReplyComment> GetAllReplyComments(int commentId)
        {
            var replyComments = _replyCommentRepository.GetMulti(x => x.Status == true && x.CommentId == commentId);
            return replyComments;
        }

        public void SaveChanges()
        {
            _unitOfWork.Commit();
        }
    }
}
