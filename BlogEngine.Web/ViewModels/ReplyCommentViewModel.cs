﻿using BlogEngine.Model.Abstracts;
using BlogEngine.Model.Models;
using System;

namespace BlogEngine.Web.ViewModels
{
    public class ReplyCommentViewModel : AuditViewModel
    {
        public Guid ReplyId { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string Content { get; set; }

        public int CountReplyComments { get; set; } = 0;

        public int CountLikes { get; set; } = 0;

        public virtual Comment Comment { get; set; }

        public int? CommentId { get; set; }
    }
}