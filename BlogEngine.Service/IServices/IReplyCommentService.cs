﻿using BlogEngine.Model.Models;
using System.Collections.Generic;

namespace BlogEngine.Service.IServices
{
    public interface IReplyCommentService
    {
        ReplyComment AddNewComment(ReplyComment replyComment);
        IEnumerable<ReplyComment> GetAllReplyComments(int commentId);
        void SaveChanges();
    }
}
