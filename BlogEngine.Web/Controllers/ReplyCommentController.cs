﻿using AutoMapper;
using BlogEngine.Common.Constants;
using BlogEngine.Model.Models;
using BlogEngine.Service.IServices;
using BlogEngine.Web.Infrastructure.Extensions;
using BlogEngine.Web.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BlogEngine.Web.Controllers
{
    public class ReplyCommentController : Controller
    {
        private IReplyCommentService _replyCommentService;
        private ICommentService _commentService;

        public ReplyCommentController(
            IReplyCommentService replyCommentService,
            ICommentService commentService)
        {
            this._replyCommentService = replyCommentService;
            this._commentService = commentService;
        }

        [HttpPost]
        public ActionResult AddNewReplyComment(string replyCommentInfo)
        {
            var errorMessage = string.Empty;
            try
            {
                var replyCommentViewModel = new JavaScriptSerializer().Deserialize<ReplyCommentViewModel>(replyCommentInfo);
                var replyComment = new ReplyComment();
                replyComment.UpdateReplyComment(replyCommentViewModel);
                replyComment = _replyCommentService.AddNewComment(replyComment);

                //Update Comment
                var commentId = replyCommentViewModel.CommentId.Value;
                Comment comment = _commentService.GetCommentById(commentId);
                comment.CountReplyComments = comment.CountReplyComments + replyCommentViewModel.CountReplyComments;
                comment.CountLikes = comment.CountLikes + replyCommentViewModel.CountLikes;
                _commentService.UpdateComment(comment);
                _replyCommentService.SaveChanges();

                // Get all reply comments
                var replyCommentsModel = _replyCommentService.GetAllReplyComments(commentId).OrderBy(x => x.CreatedDate);
                var replyCommentsViewModel = Mapper.Map<IEnumerable<ReplyComment>, IEnumerable<ReplyCommentViewModel>>(replyCommentsModel);
                return PartialView(ViewUrl._ReplyCommentArea, replyCommentsViewModel);
            }
            catch (Exception ex)
            {
                errorMessage = ex.InnerException.Message;
            }
            return RedirectToAction("Index", "Post");
        }

        [HttpPost]
        public ActionResult GetAllReplyComments(int commentId)
        {
            // Get all reply comments
            var replyCommentsModel = _replyCommentService.GetAllReplyComments(commentId);
            var replyCommentsViewModel = Mapper.Map<IEnumerable<ReplyComment>, IEnumerable<ReplyCommentViewModel>>(replyCommentsModel);
            ViewBag.CommentId = commentId;
            return PartialView(ViewUrl._ReplyCommentText, replyCommentsViewModel);
        }

        [HttpPost]
        public JsonResult UpdateReplyCommentsAndLikes(int commentId)
        {
            var errorMessage = string.Empty;
            try
            {
                var commentModel = _commentService.GetCommentById(commentId);
                var commentViewModel = Mapper.Map<Comment, CommentViewModel>(commentModel);
                var jsonData = JsonConvert.SerializeObject
                (
                    commentViewModel,
                    Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    }
                );
                return Json(new
                {
                    status = true,
                    commentVm = jsonData
                });
            }
            catch (Exception ex)
            {
                errorMessage = ex.InnerException.Message;
            }
            return Json(new 
            {
                status = false
            });
        }
    }
}