// <auto-generated />
namespace BlogEngine.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class Update_ReplyComment_SetDefaultValue_CountLikes_And_Update_Comment_SetDefaultValue_CountReplyComments_CountLikes : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Update_ReplyComment_SetDefaultValue_CountLikes_And_Update_Comment_SetDefaultValue_CountReplyComments_CountLikes));
        
        string IMigrationMetadata.Id
        {
            get { return "201808110759208_Update_ReplyComment_SetDefaultValue_CountLikes_And_Update_Comment_SetDefaultValue_CountReplyComments_CountLikes"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
