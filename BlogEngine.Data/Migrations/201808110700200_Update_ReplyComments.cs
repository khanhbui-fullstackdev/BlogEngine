namespace BlogEngine.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_ReplyComments : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ReplyComments",
                c => new
                    {
                        ReplyId = c.Guid(nullable: false),
                        Id = c.Int(nullable: false, identity: true),
                        Content = c.String(),
                        CommentId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(),
                        CreatedBy = c.String(maxLength: 255, unicode: false),
                        UpdatedBy = c.String(maxLength: 255, unicode: false),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ReplyId)
                .ForeignKey("dbo.Comments", t => t.CommentId, cascadeDelete: true)
                .Index(t => t.CommentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ReplyComments", "CommentId", "dbo.Comments");
            DropIndex("dbo.ReplyComments", new[] { "CommentId" });
            DropTable("dbo.ReplyComments");
        }
    }
}
