﻿var helperFunction = (function () {

    function test(text) {
        alert('Test:' + text);
    }

    function commentRule(commentText, ruleStart, ruleEnd) {

        var start = 0;
        var end = ruleEnd;
        var rows = 0;
        var ruleObj = {
            ruleStart: 0,
            textLength: 0,
            ruleEnd: ruleEnd,
            newLine: 0
        };
        var textLength = commentText.length;
        while (ruleObj.ruleStart < textLength) {
            if (textLength < ruleObj.ruleEnd) return ruleObj;
            else {
                rows++;
                start = start + 82;
                end = start + 81;
                if (start <= textLength && textLength <= end) {
                    ruleObj = {
                        ruleStart: start,
                        textLength: textLength,
                        ruleEnd: end,
                        newLine: rows
                    };
                    return ruleObj;
                }
            }
        }
        return ruleObj;
    }

    return {
        test: test
    };
})();