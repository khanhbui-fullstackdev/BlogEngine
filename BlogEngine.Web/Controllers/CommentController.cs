﻿using AutoMapper;
using BlogEngine.Common.Constants;
using BlogEngine.Model.Models;
using BlogEngine.Service.IServices;
using BlogEngine.Web.Infrastructure.Extensions;
using BlogEngine.Web.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BlogEngine.Web.Controllers
{
    public class CommentController : Controller
    {
        private ICommentService _commentService;
        private IPostService _postService;

        public CommentController(
            ICommentService commentService,
            IPostService postService,
            IReplyCommentService replyCommentService)
        {
            this._commentService = commentService;
            this._postService = postService;
        }

        [HttpPost]
        public JsonResult _AddNewComment(string commentInfo)
        {
            var errorMessage = string.Empty;
            try
            {
                var commentViewModel = new JavaScriptSerializer().Deserialize<CommentViewModel>(commentInfo);
                var comment = new Comment();
                comment.UpdateComment(commentViewModel);
                comment = _commentService.AddNewComment(comment);

                // Update post commment
                Post post = _postService.GetPostById(comment.PostID);
                post.CountComments = post.CountComments + commentViewModel.CountPostComments;
                post.CountLikes = post.CountLikes + commentViewModel.CountLikes;
                // Update comment view model 
                //commentViewModel.TotalComments = post.CountComments;
                //commentViewModel.TotalLikes = post.CountLikes;
                _postService.UpdatePost(post);
                _commentService.SaveChanges();

                var jsonData = JsonConvert.SerializeObject
                (
                   commentViewModel,
                   Formatting.Indented,
                   new JsonSerializerSettings
                   {
                       ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                       ContractResolver = new CamelCasePropertyNamesContractResolver()
                   }
                );
                return Json(new
                {
                    status = true,
                    commentVm = jsonData
                });
            }
            catch (Exception ex)
            {
                errorMessage = ex.InnerException.Message;
            }
            return Json(new
            {
                status = false,
                error = errorMessage
            });
        }

        [HttpPost]
        public ActionResult AddNewComment(string commentInfo)
        {
            var errorMessage = string.Empty;
            try
            {
                var commentViewModel = new JavaScriptSerializer().Deserialize<CommentViewModel>(commentInfo);
                var comment = new Comment();
                comment.UpdateComment(commentViewModel);
                comment = _commentService.AddNewComment(comment);

                // Update post commment
                Post post = _postService.GetPostById(comment.PostID);
                post.CountComments = post.CountComments + commentViewModel.CountPostComments;
                post.CountLikes = post.CountLikes + commentViewModel.CountLikes;
                _postService.UpdatePost(post);
                _commentService.SaveChanges();

                var postId = commentViewModel.PostID;
                var commentsModel = _commentService.GetCommentsByPostId(postId);
                var commentsViewModel = Mapper.Map<IEnumerable<Comment>, IEnumerable<CommentViewModel>>(commentsModel);

                return PartialView(ViewUrl._CommentArea, commentsViewModel);
            }
            catch (Exception ex)
            {
                errorMessage = ex.InnerException.Message;
            }
            return RedirectToAction("Index", "Post");
        }

        [HttpPost]
        public JsonResult UpdatePostCommentsAndLikes(int postId)
        {
            var errorMessage = string.Empty;
            try
            {
                var postModel = _postService.GetPostById(postId);
                var postViewModel = Mapper.Map<Post, PostViewModel>(postModel);

                var jsonData = JsonConvert.SerializeObject
                (
                   postViewModel,
                   Formatting.Indented,
                   new JsonSerializerSettings
                   {
                       ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                       ContractResolver = new CamelCasePropertyNamesContractResolver()
                   }
                );
                return Json(new
                {
                    status = true,
                    postVm = jsonData
                });
            }
            catch (Exception ex)
            {
                errorMessage = ex.InnerException.Message;
            }
            return Json(new
            {
                status = false
            });
        }

        [HttpPost]
        public JsonResult GetCommentsByPostId(int postId)
        {
            var errorMessage = string.Empty;
            try
            {
                var commentsModel = _commentService.GetCommentsByPostId(postId);
                var commentsViewModel = Mapper.Map<IEnumerable<Comment>, IEnumerable<CommentViewModel>>(commentsModel);

                var jsonData = JsonConvert.SerializeObject
                (
                   commentsViewModel,
                   Formatting.Indented,
                   new JsonSerializerSettings
                   {
                       ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                       ContractResolver = new CamelCasePropertyNamesContractResolver()
                   }
                );

                return Json(new
                {
                    status = true,
                    commentsVm = jsonData
                });
            }
            catch (Exception ex)
            {
                errorMessage = ex.InnerException.Message;
            }
            return Json(new
            {
                status = false,
                error = errorMessage
            });
        }       
    }
}