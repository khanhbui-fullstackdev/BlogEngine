﻿using BlogEngine.Data.Infrastrutures;
using BlogEngine.Data.Repositories.IRepositories;
using BlogEngine.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogEngine.Data.Repositories
{
    public class CommentRepository : RepositoryBase<Comment>, ICommentRepository
    {
        public CommentRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }

        public IEnumerable<Comment> GetCommentsByPostId(int postId)
        {     
            var comments = DbContext.Comments
                .Where(x => x.PostID == postId && x.Status == true)
                .OrderByDescending(x => x.CreatedDate);
            return comments;
        }
    }
}
