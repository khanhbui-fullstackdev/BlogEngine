﻿var commentController = {
    init: function () {
        commentController.registerEvents();
        commentController.customRules();
    },
    registerEvents: function () {

        // Show all comments
        $('#numberOfComments').off('click').on('click', function (e) {
            e.preventDefault();
            $('#comment-area').show();
            $('#comment-text').show();
            $('#txtName').focus();
            // Hide reply text
            $('.reply').html('');
        });

        $('#btnShowAllComments').off('click').on('click', function (e) {
            e.preventDefault();
            $('#comment-area').show();
            $('#comment-text').show();
            $('#txtName').focus();
            // Hide reply text
            $('.reply').html('');
        });

        $('#comments').off('click').on('click', function (e) {
            e.preventDefault();
            $('#comment-area').show();
            $('#comment-text').show();
            $('#txtName').focus();
            // Hide reply text
            $('.reply').html('');
        });

        $('#btnPostComment').off('click').on('click', function (e) {
            e.preventDefault();
            var isValid = $('#frm_comment').valid();
            if (isValid) {
                var name = $('#txtName').val();
                var email = $('#txtEmail').val();
                var comment = $('#txtComment').val();
                var postId = $('#txtPostId').val();
                var countPostComments = 0;
                countPostComments++;

                var commentObj = {
                    name: name,
                    userName: 'Guest',
                    content: comment,
                    email: email,
                    createdDate: new Date(),
                    createdBy: 'Guest',
                    updatedBy: null,
                    status: true,
                    countPostComments: countPostComments,
                    postID: postId
                };
                commentController.addNewComment(commentObj);
            } else {
                commentController.validatedFields();
            }
        });
        // Comment rule algorithm 
        $('#txtComment').off('keyup').on('keyup', function (e) {
            var comment = $('#txtComment').val();
            if (comment.length != 0) {
                $('#post_comment').show();
            } else {
                $('#post_comment').hide();
            }
            //var ruleObj = commentController.commentRule(comment);
            //var pasteKey = (e.ctrlKey == true && e.keyCode == 86);
            //var copyKey = (e.ctrlKey == true && e.keyCode == 67);

            //if (e.keyCode != 17 && pasteKey == true) {
            //    var actualRows = 2 + ruleObj.newLine;
            //    $('#txtComment').prop('rows', actualRows);
            //} else {
            //    var actualRows = 2 + ruleObj.newLine;
            //    $('#txtComment').prop('rows', actualRows);
            //}
        });

        // Show all reply comments
        $('.btnReplyComment').off('click').on('click', function (e) {
            e.preventDefault();
            var commentId = $(this).attr('id').split('_')[1];
            $('.reply').html('');
            $('#comment-text').hide();
            commentController.getAllReplyComments(commentId);
        });

        // Show all reply comments
        $('.small-numberOfReplyComments').off('click').on('click', function (e) {
            e.preventDefault();
            var commentId = $(this).attr('id').split('_')[1];
            $('.reply').html('');
            $('#comment-text').hide();
            commentController.getAllReplyComments(commentId);
        });

        var frm_commentConfig = {
            rules: {
                name: {
                    required: true,
                    minlength: 2,
                    maxlength: 255,
                    preventScriptInjection: true,
                    preventHtmlInjection: true,
                    preventWhiteSpace: true
                },
                email: {
                    required: true,
                    email: true,
                    minlength: 12,
                    maxlength: 50,
                    preventScriptInjection: true,
                    preventHtmlInjection: true,
                    preventWhiteSpace: true
                }
            },
            messages: {
                email: {
                    required: 'Email is required',
                    email: 'Email is invalid',
                    minlength: 'Email requires at least 12 characters',
                    maxlenght: 'Email cannot length over 50 characters',
                    preventScriptInjection: 'Email cannot contain any javascript code',
                    preventHtmlInjection: 'Email cannot contain any html characters',
                    preventWhiteSpace: 'Email cannot contain any white space or empty string'
                },
                name: {
                    required: 'Name is required',
                    minlength: 'Name requires at least 2 characters',
                    maxlength: 'Name name cannot length over 255 characters',
                    preventScriptInjection: 'Name cannot contain any javascript code',
                    preventHtmlInjection: 'Name cannot contain any html characters',
                    preventWhiteSpace: 'Name cannot contain any white space or empty string'
                },
            }
        };
        $('#txtName').off('keyup').on('keyup', function (event) {
            var nameValid = $('#txtName').valid();
            switch (nameValid) {
                case true:
                    $('#txtName').removeClass('contact-fields-textbox-error');
                    $('#txtName-error').removeClass('contact-fields-label-error');
                    break;
                case false:
                    $('#txtName').addClass('contact-fields-textbox-error');
                    $('#txtName-error').addClass('contact-fields-label-error');
                    break;
            };
        });
        $('#txtEmail').off('keyup').on('keyup', function (event) {
            var emailValid = $('#txtEmail').valid();
            switch (emailValid) {
                case true:
                    $('#txtEmail').removeClass('contact-fields-textbox-error');
                    $('#txtEmail-error').removeClass('contact-fields-label-error');
                    break;
                case false:
                    $('#txtEmail').addClass('contact-fields-textbox-error');
                    $('#txtEmail-error').addClass('contact-fields-label-error');
                    break;
            };
        });
        $('#frm_comment').validate(frm_commentConfig);
    },
    commentRule: function (comment) {
        var start = 0;
        var end = 81;
        var rows = 0;
        var ruleObj = {
            ruleStart: 0,
            textLength: 0,
            ruleEnd: 81,
            newLine: 0
        };
        var textLength = comment.length;
        while (ruleObj.ruleStart < textLength) {
            if (textLength < ruleObj.ruleEnd) return ruleObj;
            else {
                rows++;
                start = start + 82;
                end = start + 81;
                if (start <= textLength && textLength <= end) {
                    ruleObj = {
                        ruleStart: start,
                        textLength: textLength,
                        ruleEnd: end,
                        newLine: rows
                    };
                    return ruleObj;
                }
            }
        }
        return ruleObj;
    },
    addNewComment: function (comment) {
        var ajaxConfig = {
            url: '/Comment/AddNewComment',
            type: 'POST',
            dataType: 'html',
            data: {
                commentInfo: JSON.stringify(comment)
            },
            success: function (response, status, xhr) {
                if (response) {
                    // Hide comment textbox
                    $('#comment-text').hide();
                    // Hide comment button
                    $('#post_comment').hide();

                    // Set default value for text box
                    $('#txtComment').val('');
                    $('#txtName').val('');
                    $('#txtEmail').val('');

                    // After add new comment on blog/article
                    // Call Partial View _CommentArea
                    $('#comment-area').html(response);
                    var postId = $('#txtPostId').val();
                    // Update post comments and likes
                    commentController.updatePostCommentsAndLikes(postId);
                }
            }, error: function (xhr, status, error) {
                toastr.error(error, 'Error');
            }
        };
        $.ajax(ajaxConfig);
    },
    getAllReplyComments: function (commentId) {
        var ajaxConfig = {
            url: '/ReplyComment/GetAllReplyComments',
            type: 'POST',
            dataType: 'html',
            data: {
                commentId: commentId
            },
            success: function (result, status, xhr) {
                // Call Partial View _ReplyCommentText
                $('#comment_' + commentId).html(result);
                var userName = $('#lblCommentUserName_' + commentId).text();
                $('#txtReplyComment').val('@' + userName);
            },
            error: function (xhr, status, error) {
                toastr.error(error);
            }
        };
        $.ajax(ajaxConfig);
    },
    updatePostCommentsAndLikes: function (postId) {
        var ajaxConfig = {
            url: '/Comment/UpdatePostCommentsAndLikes',
            type: 'POST',
            dataType: 'JSON',
            data: {
                postId: postId
            },
            success: function (response, status, xhr) {
                if (response.status) {
                    var postViewModel = JSON.parse(response.postVm);
                    var totalComments = postViewModel.countComments;
                    if (totalComments > 1) {
                        // Update total comments on UI
                        $('#numberOfComments').text(totalComments + ' comments');
                        $('#small-numberOfComments').text(totalComments + ' comments');
                    }
                    else {
                        // Update total comments on UI
                        $('#numberOfComments').text(totalComments + ' comment');
                        $('#small-numberOfComments').text(totalComments + ' comment');
                    }
                } else {
                    toastr.error(response.error);
                }
            },
            error: function (xhr, status, error) {
                toastr.error(error);
            }
        };
        $.ajax(ajaxConfig);
    },
    validatedFields: function () {
        var nameValid = $('#txtName').valid();
        var emailValid = $('#txtEmail').valid();
        var replyCommentValid = $('#txtComment').valid();

        switch (nameValid) {
            case true:
                $('#txtName').removeClass('contact-fields-textbox-error');
                $('#txtName-error').removeClass('contact-fields-label-error');
                break;
            case false:
                $('#txtName').addClass('contact-fields-textbox-error');
                $('#txtName-error').addClass('contact-fields-label-error');
                break;
        };
        switch (emailValid) {
            case true:
                $('#txtEmail').removeClass('contact-fields-textbox-error');
                $('#txtEmail-error').removeClass('contact-fields-label-error');
                break;
            case false:
                $('#txtEmail').addClass('contact-fields-textbox-error');
                $('#txtEmail-error').addClass('contact-fields-label-error');
                break;
        };
        switch (replyCommentValid) {
            case true:
                $('#txtComment').removeClass('contact-fields-textbox-error');
                $('#txtComment-error').removeClass('contact-fields-label-error');
                break;
            case false:
                $('#txtComment').addClass('contact-fields-textbox-error');
                $('#txtComment-error').addClass('contact-fields-label-error');
                break;
        };
    },
    customRules: function () {
        jQuery.validator.addMethod('preventScriptInjection', function (value, element, params) {
            if (value.toLowerCase().search('<script>') !== -1 || value.toLowerCase().search('</script>') !== -1)
                return false; // has script
            return true; // none script
        }, '');
        jQuery.validator.addMethod('preventHtmlInjection', function (value, element, params) {
            var htmlPattern = new RegExp(/<[a-z][\s\S]*>/i);
            if (htmlPattern.test(value))
                return false; // has html
            return true; // none html
        }, '');
        jQuery.validator.addMethod('preventWhiteSpace', function (value, element, params) {
            if (value.trim() === '')
                return false; // has empty string
            return true; // none empty string
        }, '');
    }
};
commentController.init();