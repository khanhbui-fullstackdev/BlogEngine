﻿using BlogEngine.Model.Models;
using System.Collections.Generic;

namespace BlogEngine.Service.IServices
{
    public interface ICommentService
    {
        Comment AddNewComment(Comment comment);
        IEnumerable<Comment> GetCommentsByPostId(int postId);
        Comment GetCommentById(int commentId);
        void UpdateComment(Comment comment);
        void SaveChanges();
    }
}
