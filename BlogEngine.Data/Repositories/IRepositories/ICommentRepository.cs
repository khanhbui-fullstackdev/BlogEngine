﻿using BlogEngine.Data.Infrastrutures;
using BlogEngine.Model.Models;
using System.Collections.Generic;

namespace BlogEngine.Data.Repositories.IRepositories
{
    public interface ICommentRepository : IRepositoryBase<Comment>
    {
        IEnumerable<Comment> GetCommentsByPostId(int postId);
    }
}
