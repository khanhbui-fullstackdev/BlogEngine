﻿var commentAreaController = {
    init: function () {
        commentAreaController.registerEvents();
    },
    registerEvents: function () {
        $('.btnReplyComment').off('click').on('click', function (e) {
            e.preventDefault();
            var commentId = $(this).attr('id').split('_')[1];
            $('.reply').html('');
            $('#comment-text').hide();
            commentAreaController.getAllReplyComments(commentId);
        });
        // Show all reply comments
        $('.small-numberOfReplyComments').off('click').on('click', function (e) {
            e.preventDefault();
            var commentId = $(this).attr('id').split('_')[1];
            $('.reply').html('');
            $('#comment-text').hide();
            commentAreaController.getAllReplyComments(commentId);
        });
    },
    getAllReplyComments: function (commentId) {
        var ajaxConfig = {
            url: '/ReplyComment/GetAllReplyComments',
            type: 'POST',
            dataType: 'html',
            data: {
                commentId: commentId
            },
            success: function (result, status, xhr) {
                // Call Partial View _ReplyCommentText
                $('#comment_' + commentId).html(result);
            },
            error: function (xhr, status, error) {
                toastr.error(error);
            }
        };
        $.ajax(ajaxConfig);
    },
};
commentAreaController.init();