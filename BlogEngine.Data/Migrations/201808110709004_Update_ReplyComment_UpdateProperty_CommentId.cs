namespace BlogEngine.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_ReplyComment_UpdateProperty_CommentId : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ReplyComments", "CommentId", "dbo.Comments");
            DropIndex("dbo.ReplyComments", new[] { "CommentId" });
            AlterColumn("dbo.ReplyComments", "CommentId", c => c.Int());
            CreateIndex("dbo.ReplyComments", "CommentId");
            AddForeignKey("dbo.ReplyComments", "CommentId", "dbo.Comments", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ReplyComments", "CommentId", "dbo.Comments");
            DropIndex("dbo.ReplyComments", new[] { "CommentId" });
            AlterColumn("dbo.ReplyComments", "CommentId", c => c.Int(nullable: false));
            CreateIndex("dbo.ReplyComments", "CommentId");
            AddForeignKey("dbo.ReplyComments", "CommentId", "dbo.Comments", "ID", cascadeDelete: true);
        }
    }
}
