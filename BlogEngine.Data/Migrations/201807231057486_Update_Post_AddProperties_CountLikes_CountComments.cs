namespace BlogEngine.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Post_AddProperties_CountLikes_CountComments : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Posts", "CountLikes", c => c.Int(nullable: false));
            AddColumn("dbo.Posts", "CountComments", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Posts", "CountComments");
            DropColumn("dbo.Posts", "CountLikes");
        }
    }
}
