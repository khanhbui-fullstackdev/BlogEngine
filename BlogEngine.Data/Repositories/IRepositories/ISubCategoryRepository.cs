﻿using BlogEngine.Data.Infrastrutures;
using BlogEngine.Model.Models;

namespace BlogEngine.Data.Repositories.IRepositories
{
    public interface ISubCategoryRepository : IRepositoryBase<SubCategory>
    {

    }
}
