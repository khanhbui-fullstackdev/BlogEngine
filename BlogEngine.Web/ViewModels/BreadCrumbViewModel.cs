﻿namespace BlogEngine.Web.ViewModels
{
    public class BreadCrumbViewModel
    {
        public int CategoryId { get; set; }

        public int SubCategoryId { get; set; }

        public string Name { get; set; }
    }
}