﻿using BlogEngine.Data.Infrastrutures;
using BlogEngine.Data.Repositories.IRepositories;
using BlogEngine.Model.Models;
using BlogEngine.Service.IServices;
using System.Collections.Generic;

namespace BlogEngine.Service
{
    public class CommentService : ICommentService
    {
        private ICommentRepository _commentRepository;
        private IUnitOfWork _unitOfWork;

        public CommentService(IUnitOfWork unitOfWork, ICommentRepository commentRepository)
        {
            this._unitOfWork = unitOfWork;
            this._commentRepository = commentRepository;
        }

        public Comment AddNewComment(Comment comment)
        {
            return _commentRepository.Add(comment);
        }

        public Comment GetCommentById(int commentId)
        {
            return _commentRepository.GetSingleByCondition(x => x.ID == commentId && x.Status == true);
        }

        public IEnumerable<Comment> GetCommentsByPostId(int postId)
        {
            var comments = _commentRepository.GetCommentsByPostId(postId);
            return comments;
        }

        public void SaveChanges()
        {
            _unitOfWork.Commit();
        }

        public void UpdateComment(Comment comment)
        {
            _commentRepository.Update(comment);
        }
    }
}
