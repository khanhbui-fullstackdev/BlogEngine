﻿namespace BlogEngine.Common.Constants
{
    public class Config
    {
        public const string Administrator = "Administrator";

        public static string PageSize = "PageSize";
        public static string MaxPage = "MaxPage";
        public static string AdminEmail = "AdminEmail";
        public static string CurrentUrl = "CurrentUrl";

        public static string SuccessMessage = "SuccessMessage";

        public static string SMTPHost = "SMTPHost";
        public static string SMTPPort = "SMTPPort";
        public static string FromEmailAddress = "FromEmailAddress";
        public static string FromEmailPassword = "FromEmailPassword";
        public static string FromName = "FromName";
    }
}
