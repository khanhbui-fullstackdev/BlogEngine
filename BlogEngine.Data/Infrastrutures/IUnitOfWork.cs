﻿namespace BlogEngine.Data.Infrastrutures
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
