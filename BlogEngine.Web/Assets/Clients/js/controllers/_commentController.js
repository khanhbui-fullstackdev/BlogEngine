﻿var _commentController = {
    init: function () {
        _commentController.registerEvents();
    },
    registerEvents: function () {
        $('#comments').off('click').on('click', function (e) {
            e.preventDefault();// prevent default action of tag a that is jumping to the top after click          
            $('#comment-text').show();
            $('#txtComment').focus();
        });
        $('#txtComment').off('keyup').on('keyup', function (e) {
            var comment = $('#txtComment').val();
            if (comment.length != 0) {
                $('#post_comment').show();
            } else {
                $('#post_comment').hide();
            }
            var ruleObj = _commentController.commentRule(comment);
            var pasteKey = (e.ctrlKey == true && e.keyCode == 86);
            var copyKey = (e.ctrlKey == true && e.keyCode == 67);

            if (e.keyCode != 17 && pasteKey == true) {
                var actualRows = 2 + ruleObj.newLine;
                $('#txtComment').prop('rows', actualRows);
            } else {
                var actualRows = 2 + ruleObj.newLine;
                $('#txtComment').prop('rows', actualRows);
            }
        });
        $('#btnPostComment').off('click').on('click', function (e) {
            e.preventDefault();
            var commentText = $('#txtComment').val();
            var postId = $('#txtPostId').val();
            var countComments = 0;
            countComments++;
            var comment = {
                name: 'Guest',
                userName: 'Guest',
                content: commentText,
                email: null,
                createdDate: new Date(),
                updatedBy: null,
                status: true,
                countComments: countComments,
                postID: postId
            };
            // Call ajax method
            _commentController.addNewComment(comment);
        });
        $('#btnReply').off('click').on('click', function (e) {
            e.preventDefault();
        });
        $('#btnLike').off('click').on('click', function (e) {
            e.preventDefault();
        });
        $('#btnCountComments').off('click').on('click', function (e) {
            e.preventDefault();
            var postId = $('#txtPostId').val();
            _commentController.getCommentsByPostId(postId);
        });

        //  using on in JQuery, when we want to trigger events on elements 
        //  which are dynamically inserted in DOM

        /* The root cause of issue
         * Event handlers are bound only to the currently selected elements; 
         * they must exist on the page at the time your code makes the call to .on(). 
         * To ensure the elements are present and can be selected, 
         * perform event binding inside a document ready handler for elements 
         * that are in the HTML markup on the page. 
         * If new HTML is being injected into the page, 
         * select the elements and attach event handlers after the new HTML is placed into the page. 
         * Or, use delegated events to attach an event handler, as described next.
         * 
         * https://stackoverflow.com/questions/18196185/jquery-click-event-doesnt-work-after-append-dont-know-how-to-use-on/18196235
         * **/

        $("#comment-area").off('click').on("click", ".btnReplyComment", function (event) {
            event.preventDefault();
            var commentId = $(this).attr('id');
            $('#comment-text').show();

        });
    },
    commentRule: function (comment) {
        var start = 0;
        var end = 81;
        var rows = 0;
        var ruleObj = {
            ruleStart: 0,
            textLength: 0,
            ruleEnd: 81,
            newLine: 0
        };
        var textLength = comment.length;
        while (ruleObj.ruleStart < textLength) {
            if (textLength < ruleObj.ruleEnd) return ruleObj;
            else {
                rows++;
                start = start + 82;
                end = start + 81;
                if (start <= textLength && textLength <= end) {
                    ruleObj = {
                        ruleStart: start,
                        textLength: textLength,
                        ruleEnd: end,
                        newLine: rows
                    };
                    return ruleObj;
                }
            }
        }
        return ruleObj;
    },
    emptyObject: function (obj) {
        for (var key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    },
    addNewComment: function (comment) {
        var ajaxConfig = {
            url: '/Comment/AddNewComment',
            type: 'POST',
            dataType: 'json',
            //contentType: 'application/json; charset=utf-8',
            data: {
                commentInfo: JSON.stringify(comment)
            },
            success: function (response, status, xhr) {
                if (response.status) {
                    var commentViewModel = JSON.parse(response.commentVm);
                    var commentContent = commentViewModel.content;
                    var totalComments = commentViewModel.totalComments;
                    var userName = commentViewModel.userName;
                    var createdDate = commentViewModel.createdDate;

                    $('#comment-area').show();//show tag div
                    if (totalComments > 1) {
                        $('#numberOfComments').text(totalComments + ' comments');
                    } else {
                        $('#numberOfComments').text(totalComments + ' comment');
                    }
                    $('#comment-text').show();
                    //Hide comment textbox
                    $('#comment-text').hide();
                    $('#post_comment').hide();
                    $('#txtComment').val('');
                } else {
                    toastr.error(response.error, 'Error');
                }
            }, error: function (xhr, status, error) {
                toastr.error(error, 'Error');
            }
        }
        $.ajax(ajaxConfig);
    },
    updateComment: function () {
        // Edit comment
    },
    getCommentsByPostId: function (postId) {
        var ajaxConfig = {
            url: '/Comment/GetCommentsByPostId',
            type: 'POST',
            dataType: 'json',
            data: {
                postId: postId
            },
            success: function (response, status, xhr) {
                if (response.status) {
                    var commentsViewModel = JSON.parse(response.commentsVm);
                    var templateHtml = '';
                    $.each(commentsViewModel, function (index, element) {
                        var dateTime = new Date(element.createdDate);
                        var day = dateTime.getDate() < 10 ? '0' + dateTime.getDate() : dateTime.getDate();
                        var month = (dateTime.getMonth() + 1) < 10 ? '0' + (dateTime.getMonth() + 1) : dateTime.getMonth();
                        var year = dateTime.getFullYear();
                        var createdDate = day + '/' + month + '/' + year + ' at ' + dateTime.getHours() + ":" + (dateTime.getMinutes() < 10 ? '0' + dateTime.getMinutes() : dateTime.getMinutes());

                        templateHtml += '<div class="form-group comment-area">';
                        templateHtml += '<img src="/Assets/Clients/images/about-me.jpg" class="img-circle navbar-left" alt="No name" width="50" height="50" />';
                        templateHtml += '<div class="col-sm-11">';
                        templateHtml += '<ul class="list-unstyled">';
                        templateHtml += '<li><small><a href="#" id="lblUserName">' + element.userName + '</a></small></li>';
                        templateHtml += '<li id="lblCreatedDate">' + createdDate + '</li></ul>';
                        templateHtml += '</div>';
                        templateHtml += '<div class="col-sm-11 comment-text-display">';
                        templateHtml += '<p id="comment-display">' + element.content + '</p>';
                        templateHtml += '<ul id="likesAndComments" class="list-unstyled nav-pills">';
                        templateHtml += '<li class="nav-item"><a class="btnLikeComment" name="likeComment" href="#" id="' + element.id + '">Like<i class="fa fa-thumbs-up"></i></a></li>';
                        templateHtml += '<li class="nav-item"><a class="btnReplyComment" name="replyComment" href="#" id="' + element.id + '">Reply<i class="fa fa-comments"></i></a></li>';
                        templateHtml += '</ul></div></div>';
                    });
                    $('#comment-area').append(templateHtml);
                    $('#comment-area').show();//show tag div
                } else {
                    toastr.error(response.error, 'Error');
                }
            },
            error: function (xhr, status, error) {
                toastr.error(error, 'Error');
            }
        };
        $.ajax(ajaxConfig);
    },
    replyComment: function (commentId) {

    }
};
_commentController.init();