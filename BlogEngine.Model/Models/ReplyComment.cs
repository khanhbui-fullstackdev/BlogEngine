﻿using BlogEngine.Common.Constants;
using BlogEngine.Model.Abstracts;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogEngine.Model.Models
{
    [Table("ReplyComments")]
    public class ReplyComment : Audit
    {
        [Key]
        public Guid ReplyId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(255), Column(TypeName = "nvarchar")]
        public string Name { get; set; }

        [StringLength(300), Column(TypeName = "varchar")]
        public string UserName { get; set; }

        [EmailAddress(ErrorMessage = ErrorMessage.InvalidEmail), StringLength(50), Column(TypeName = "varchar")]
        public string Email { get; set; }

        public string Content { get; set; }

        public int CountLikes { get; set; } = 0;

        [ForeignKey("CommentId")]
        public virtual Comment Comment { get; set; }

        public int? CommentId { get; set; }
    }
}
