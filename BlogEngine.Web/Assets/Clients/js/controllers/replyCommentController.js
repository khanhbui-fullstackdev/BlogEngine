﻿var replyCommentController = {
    init: function () {
        replyCommentController.registerEvents();
        replyCommentController.customRules();
        replyCommentController.setDefaultValueForReplyCommentText();
    },
    registerEvents: function () {
        // Comment rule
        $('#txtReplyComment').off('keyup').on('keyup', function (e) {
            var replyComment = $('#txtReplyComment').val();
            if (replyComment.length != 0) {
                // Show the button area when it meets condition
                $('#reply-comment').show();
            } else {
                // Hide the button area when it meets condition
                $('#reply-comment').hide();
            }
        });

        var frm_replyCommentConfig = {
            rules: {
                replyName: {
                    required: true,
                    minlength: 2,
                    maxlength: 255,
                    preventScriptInjection: true,
                    preventHtmlInjection: true,
                    preventWhiteSpace: true
                },
                replyEmail: {
                    required: true,
                    email: true,
                    minlength: 12,
                    maxlength: 50,
                    preventScriptInjection: true,
                    preventHtmlInjection: true,
                    preventWhiteSpace: true
                },
                replyComment: {
                    required: true
                }
            },
            messages: {
                replyEmail: {
                    required: 'Email is required',
                    email: 'Email is invalid',
                    minlength: 'Email requires at least 12 characters',
                    maxlenght: 'Email cannot length over 50 characters',
                    preventScriptInjection: 'Email cannot contain any javascript code',
                    preventHtmlInjection: 'Email cannot contain any html characters',
                    preventWhiteSpace: 'Email cannot contain any white space or empty string'
                },
                replyName: {
                    required: 'Name is required',
                    minlength: 'Name requires at least 2 characters',
                    maxlength: 'Name name cannot length over 255 characters',
                    preventScriptInjection: 'Name cannot contain any javascript code',
                    preventHtmlInjection: 'Name cannot contain any html characters',
                    preventWhiteSpace: 'Name cannot contain any white space or empty string'
                },
                replyComment: {
                    required: 'Comment is required'
                }
            }
        };
        $('#txtReplyName').off('keyup').on('keyup', function (event) {
            var nameValid = $('#txtReplyName').valid();
            switch (nameValid) {
                case true:
                    $('#txtReplyName').removeClass('contact-fields-textbox-error');
                    $('#txtReplyName-error').removeClass('contact-fields-label-error');
                    break;
                case false:
                    $('#txtReplyName').addClass('contact-fields-textbox-error');
                    $('#txtReplyName-error').addClass('contact-fields-label-error');
                    break;
            };
        });
        $('#txtReplyEmail').off('keyup').on('keyup', function (event) {
            var emailValid = $('#txtReplyEmail').valid();
            switch (emailValid) {
                case true:
                    $('#txtReplyEmail').removeClass('contact-fields-textbox-error');
                    $('#txtReplyEmail-error').removeClass('contact-fields-label-error');
                    break;
                case false:
                    $('#txtReplyEmail').addClass('contact-fields-textbox-error');
                    $('#txtReplyEmail-error').addClass('contact-fields-label-error');
                    break;
            };
        });
        $('#frm_replyComment').validate(frm_replyCommentConfig);

        // Post a comment
        $('#btnPostReplyComment').off('click').on('click', function (e) {
            e.preventDefault();
            replyCommentController.saveChanges();
        });
        // Reply person comment
        $('.btnReplyPersonComment').off('click').on('click', function (e) {
            e.preventDefault();
            var commentId = $('#txtCommentId').val();
            var replyCommentId = parseInt($(this).attr('id').split('_')[1]);
            var replyCommentName = $('#lblReplyCommentUserName_' + replyCommentId).text();

            $('#frm_replyComment').show();
            $('#txtReplyName').focus();
            $('#txtReplyComment').val('@' + replyCommentName);
        });
    },
    addNewReplyComment: function (replyCommentInfo) {
        debugger;
        var commentId = replyCommentInfo['commentId'];
        var ajaxConfig = {
            url: '/ReplyComment/AddNewReplyComment',
            type: 'POST',
            dataType: 'html',
            data: {
                replyCommentInfo: JSON.stringify(replyCommentInfo)
            },
            success: function (response, status, xhr) {
                $.when($('#replyComment-area').html(response))
                    .then(replyCommentController.updateReplyCommentsAndLikes(commentId));
                // Hide Reply Comment Text section
                $('#frm_replyComment').hide();
            },
            error: function (xhr, status, error) {
                toastr.error(error);
            }
        };
        $.ajax(ajaxConfig);
    },
    updateReplyCommentsAndLikes: function (commentId) {
        var ajaxConfig = {
            url: '/ReplyComment/UpdateReplyCommentsAndLikes',
            type: 'POST',
            dataType: 'JSON',
            data: {
                commentId: commentId
            },
            success: function (response, status, xhr) {
                if (response.status) {
                    var commentViewModel = JSON.parse(response.commentVm);
                    var countReplyComments = commentViewModel.countReplyComments;
                    if (countReplyComments > 1) {
                        // Update reply comments on UI
                        $('#small-numberOfReplyComments_' + commentId).text(countReplyComments + ' Replies');
                    } else {
                        // Update reply comments on UI
                        $('#small-numberOfReplyComments_' + commentId).text(countReplyComments + ' Reply');
                    }
                } else {
                    toastr.error(response.error, 'Error');
                }
            },
            error: function (xhr, status, error) {
                toastr.error(error, 'Error');
            }
        };
        $.ajax(ajaxConfig);
    },
    saveChanges: function () {
        var isValid = $('#frm_replyComment').valid();
        if (isValid) {
            var replyName = $('#txtReplyName').val();
            var replyEmail = $('#txtReplyEmail').val();
            var replyComment = $('#txtReplyComment').val();
            var commentId = $('#txtCommentId').val();
            var countReplyComments = 0;
            countReplyComments++;

            var replyCommentObj = {
                name: replyName,
                userName: 'Guest',
                email: replyEmail,
                content: replyComment,
                createdDate: new Date(),
                createdBy: 'Guest',
                updateBy: null,
                status: true,
                countReplyComments: countReplyComments,
                commentId: commentId,
            };
            replyCommentController.addNewReplyComment(replyCommentObj);
        } else {
            replyCommentController.validatedFields();
        }
    },
    setDefaultValueForReplyCommentText: function () {
        $('#txtReplyName').val('');
        $('#txtReplyEmail').val('');
        $('#txtReplyComment').val('');

        $('#txtReplyName').focus();
    },
    validatedFields: function () {
        var nameValid = $('#txtReplyName').valid();
        var emailValid = $('#txtReplyEmail').valid();
        var replyCommentValid = $('#txtReplyComment').valid();

        switch (nameValid) {
            case true:
                $('#txtReplyName').removeClass('contact-fields-textbox-error');
                $('#txtReplyName-error').removeClass('contact-fields-label-error');
                break;
            case false:
                $('#txtReplyName').addClass('contact-fields-textbox-error');
                $('#txtReplyName-error').addClass('contact-fields-label-error');
                break;
        };
        switch (emailValid) {
            case true:
                $('#txtReplyEmail').removeClass('contact-fields-textbox-error');
                $('#txtReplyEmail-error').removeClass('contact-fields-label-error');
                break;
            case false:
                $('#txtReplyEmail').addClass('contact-fields-textbox-error');
                $('#txtReplyEmail-error').addClass('contact-fields-label-error');
                break;
        };
        switch (replyCommentValid) {
            case true:
                $('#txtReplyComment').removeClass('contact-fields-textbox-error');
                $('#txtReplyComment-error').removeClass('contact-fields-label-error');
                break;
            case false:
                $('#txtReplyComment').addClass('contact-fields-textbox-error');
                $('#txtReplyComment-error').addClass('contact-fields-label-error');
                break;
        };
    },
    customRules: function () {
        jQuery.validator.addMethod('preventScriptInjection', function (value, element, params) {
            if (value.toLowerCase().search('<script>') !== -1 || value.toLowerCase().search('</script>') !== -1)
                return false; // has script
            return true; // none script
        }, '');
        jQuery.validator.addMethod('preventHtmlInjection', function (value, element, params) {
            var htmlPattern = new RegExp(/<[a-z][\s\S]*>/i);
            if (htmlPattern.test(value))
                return false; // has html
            return true; // none html
        }, '');
        jQuery.validator.addMethod('preventWhiteSpace', function (value, element, params) {
            if (value.trim() === '')
                return false; // has empty string
            return true; // none empty string
        }, '');
    }
};
replyCommentController.init();