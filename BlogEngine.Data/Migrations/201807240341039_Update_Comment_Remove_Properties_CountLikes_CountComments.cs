namespace BlogEngine.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Comment_Remove_Properties_CountLikes_CountComments : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Comments", "CountLikes");
            DropColumn("dbo.Comments", "CountComments");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Comments", "CountComments", c => c.Int(nullable: false));
            AddColumn("dbo.Comments", "CountLikes", c => c.Int(nullable: false));
        }
    }
}
