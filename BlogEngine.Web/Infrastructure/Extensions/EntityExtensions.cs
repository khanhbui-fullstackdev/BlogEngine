﻿using BlogEngine.Model.Models;
using BlogEngine.Web.ViewModels;
using System;

namespace BlogEngine.Web.Infrastructure.Extensions
{
    /// <summary>
    /// Extention Method of class PostCategory 
    /// It must be static class and static method
    /// </summary>
    /// <param name="Post"></param>
    /// <param name="PostViewModel"></param>
    public static class EntityExtensions
    {
        public static void UpdatePost(this Post post, PostViewModel postViewModel)
        {
            post.ID = postViewModel.ID;
            post.Name = postViewModel.Name;
            post.Slug = postViewModel.Slug;
            post.Summary = postViewModel.Summary;
        }

        public static void UpdateContact(this Contact contact, ContactViewModel contactViewModel)
        {
            contact.ContactId = contactViewModel.ContactId;
            contact.ContactName = contactViewModel.ContactName;
            contact.ContactEmail = contactViewModel.ContactEmail;
            contact.Content = contactViewModel.Content;
            contact.CreatedDate = DateTime.Now;
        }

        public static void UpdateComment(this Comment comment, CommentViewModel commentViewModel)
        {
            comment.ID = commentViewModel.ID;
            comment.Name = commentViewModel.Name;
            comment.UserName = commentViewModel.UserName;
            comment.Content = commentViewModel.Content;
            comment.Status = commentViewModel.Status;
            comment.CreatedBy = commentViewModel.CreatedBy;
            comment.CreatedDate = commentViewModel.CreatedDate;
            comment.PostID = commentViewModel.PostID;
            comment.UpdatedBy = commentViewModel.UpdatedBy;
            comment.Email = commentViewModel.Email;
        }

        public static void UpdateReplyComment(this ReplyComment replyComment, ReplyCommentViewModel replyCommentViewModel)
        {
            replyComment.Id = replyCommentViewModel.Id;
            replyComment.ReplyId = Guid.NewGuid();
            replyComment.Name = replyCommentViewModel.Name;
            replyComment.UserName = replyCommentViewModel.UserName;
            replyComment.Content = replyCommentViewModel.Content;
            replyComment.Status = replyCommentViewModel.Status;
            replyComment.CreatedBy = replyCommentViewModel.CreatedBy;
            replyComment.CreatedDate = replyCommentViewModel.CreatedDate;
            replyComment.CommentId = replyCommentViewModel.CommentId;
            replyComment.UpdatedBy = replyCommentViewModel.UpdatedBy;
            replyComment.Email = replyCommentViewModel.Email;
        }
    }
}