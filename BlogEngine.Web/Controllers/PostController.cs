﻿using AutoMapper;
using BlogEngine.Model.Models;
using BlogEngine.Service.IServices;
using BlogEngine.Web.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BlogEngine.Web.Controllers
{
    public class PostController : Controller
    {
        private IPostService _postService;
        private ICommentService _commentService;

        public PostController(
            IPostService postService,
            ICommentService commentService
            )
        {
            this._postService = postService;
            this._commentService = commentService;
        }

        // GET: Post
        public ActionResult _Index(int id)
        {
            var postModel = _postService.GetPostById(id);
            var postViewModel = Mapper.Map<Post, PostViewModel>(postModel);
            
            ViewBag.CategoryId = postViewModel.CategoryID;
            return View("_Index", postViewModel);
        }

        // GET: Post
        public ActionResult Index(int id)
        {
            var postModel = _postService.GetPostById(id);
            var postViewModel = Mapper.Map<Post, PostViewModel>(postModel);

            var commentsModel = _commentService.GetCommentsByPostId(id);
            var commentsViewModel = Mapper.Map<IEnumerable<Comment>, IEnumerable<CommentViewModel>>(commentsModel);

            PostCommentViewModel postCommentViewModel = new PostCommentViewModel
            {
                Comments = commentsViewModel,
                Post = postViewModel
            };
            ViewBag.CategoryId = postViewModel.CategoryID;
            return View(postCommentViewModel);
        }

        public ActionResult PostsByCategory(int id)
        {
            var postsModel = _postService.GetPostsByCategory(id);
            var postsViewModel = Mapper.Map<IEnumerable<Post>, IEnumerable<PostViewModel>>(postsModel);
            ViewBag.CategoryId = id;
            return View(postsViewModel);
        }

        public ActionResult PostsBySubCategory(int id)
        {
            var postsModel = _postService.GetPostsBySubcategory(id);
            var postsViewModel = Mapper.Map<IEnumerable<Post>, IEnumerable<PostViewModel>>(postsModel);
            ViewBag.SubCategoryId = id;
            return View(postsViewModel);
        }


        public ActionResult PostsByKeyword(string keyword)
        {
            var postsModel = _postService.GetPostsByKeyword(keyword);
            var postsViewModel = Mapper.Map<IEnumerable<Post>, IEnumerable<PostViewModel>>(postsModel);
            return View(postsViewModel);
        }
    }
}