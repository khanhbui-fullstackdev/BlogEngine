﻿using BlogEngine.Data.Infrastrutures;
using BlogEngine.Data.Repositories.IRepositories;
using BlogEngine.Model.Models;

namespace BlogEngine.Data.Repositories
{
    public class ReplyCommentRepository : RepositoryBase<ReplyComment>, IReplyCommentRepository
    {
        public ReplyCommentRepository(IDbFactory dbFactory) : base(dbFactory)
        {

        }
    }
}
