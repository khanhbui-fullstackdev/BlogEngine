namespace BlogEngine.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_ReplyComment_AddProperty_CountLikes_And_Update_Comment_AddProperty_CountReplyComments_CountLikes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Comments", "CountReplyComments", c => c.Int(nullable: false));
            AddColumn("dbo.Comments", "CountLikes", c => c.Int(nullable: false));
            AddColumn("dbo.ReplyComments", "CountLikes", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ReplyComments", "CountLikes");
            DropColumn("dbo.Comments", "CountLikes");
            DropColumn("dbo.Comments", "CountReplyComments");
        }
    }
}
