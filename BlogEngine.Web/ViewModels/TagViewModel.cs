﻿namespace BlogEngine.Web.ViewModels
{
    public class TagViewModel
    {
        public int ID { set; get; }

        public string Name { set; get; }

        public string Type { set; get; }

        public string Href { get; set; }
    }
}