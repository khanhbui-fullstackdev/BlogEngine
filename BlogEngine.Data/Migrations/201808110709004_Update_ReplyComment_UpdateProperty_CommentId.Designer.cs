// <auto-generated />
namespace BlogEngine.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class Update_ReplyComment_UpdateProperty_CommentId : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Update_ReplyComment_UpdateProperty_CommentId));
        
        string IMigrationMetadata.Id
        {
            get { return "201808110709004_Update_ReplyComment_UpdateProperty_CommentId"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
